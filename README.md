# lxdist-vm-rebuild

This repository stores the simple bash script `rebuild-lxdist-testing-vm.sh` which is to be used to rebuild the testing VMs used for CentOS distribution release testing (see [https://linuxops.web.cern.ch/distributions/architecture](https://linuxops.web.cern.ch/distributions/architecture) for more details)

The basic principles for this architecture are that the testing VMs have:

* been defined in OpenStack with an iPXE image that boots to AIMS2
* registered to AIMS2 with the kickstarts found in this repository
* can be safely rebuilt at any time with the `rebuild-lxdist-testing-vms.sh` script

Notes:

* `cern-linuxsupport-access` is installed and enabled via the kickstart of these VMs
* rebuilding a VM will install the latest version packages available, however no 'testing' packages will be installed
