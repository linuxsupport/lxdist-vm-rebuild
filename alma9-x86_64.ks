#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Use network installation
url --url="https://linuxsoft.cern.ch/cern/alma/9/BaseOS/x86_64/os/"

# License agreement
eula --agreed
# Use text mode install
text
firstboot --disable
ignoredisk --only-use=vda
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8

# Network information
network  --bootproto=dhcp --device=link --activate --noipv6
network  --hostname=localhost.localdomain
# Reboot after installation
reboot
# Root password
rootpw --iscrypted [this-is-not-a-root-password]
# SELinux configuration
selinux --enforcing
# Do not configure the X Window System
skipx
# System timezone
timezone Europe/Zurich --isUtc
# System bootloader configuration
bootloader --append="console=ttyS0,115200 console=tty0 crashkernel=auto" --location=mbr --timeout=5 --boot-drive=vda
# Clear the Master Boot Record
zerombr
# Partition clearing information
clearpart --all --initlabel
# Disk partitioning information
part / --fstype="xfs" --ondisk=vda --size=15000 --mkfsoptions="-n ftype=1"

%post --logfile=/root/anaconda-post.log

# lock root account
passwd -d root
passwd -l root

rpm -e linux-firmware
yum -y update

# cannot login on the console anyway ...
sed -i 's/^#NAutoVTs=.*/NAutoVTs=0/' /etc/systemd/logind.conf
# installation time shutdown problem ?
systemctl restart systemd-logind

# disable cloud-init managing network (new in 7.4)
cat >> /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg << EOF
network: {config: disabled}
EOF

# make the system reask for ip .. just in case
cat >> /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
DHCPV6C=yes
PERSISTENT_DHCLIENT=1
NOZEROCONF=1
EOF

sed -i 's|^HWADDR=.*||' /etc/sysconfig/network-scripts/ifcfg-eth0
rm -f /etc/udev/rules.d/70-persistent-net.rules


# set virtual-guest as default profile for tuned
tuned-adm profile virtual-guest

# fix rtc to use utc not local time (INC0974179)
# done via --utc timzezone switch
#timedatectl set-local-rtc 0
#cat >> /etc/adjtime << EOF
#0.0 0 0.0
#0
#UTC
#EOF

# and identical chrony.keys (INC0980266)
echo "" > /etc/chrony.keys

#no tmpfs for /tmp."
systemctl mask tmp.mount

# root - enabled, cloud user - disabled.
if [ -e /etc/cloud/cloud.cfg ]; then
    sed -i 's|^disable_root.*|disable_root: 0|' /etc/cloud/cloud.cfg
    sed -i 's|^ssh_deletekeys.*|ssh_deletekeys: 0|' /etc/cloud/cloud.cfg
    sed -i 's|\- default||' /etc/cloud/cloud.cfg
    sed -i 's|^users:||' /etc/cloud/cloud.cfg

    # fix cloud-init (INC0974035 RH 01594925)
    sed -i -e 's/\&\s\~/\& stop/' /etc/rsyslog.d/21-cloudinit.conf
    # and fix it again !
    sed -i -e 's/\[CLOUDINIT\]/cloud-init/' /etc/rsyslog.d/21-cloudinit.conf
    sed -i -e 's/syslogtag/programname/' /etc/rsyslog.d/21-cloudinit.conf
    sed -i -e 's/cloud-init.log/cloud-init-output.log/' /etc/rsyslog.d/21-cloudinit.conf
fi

dnf --repofrompath=almacern,https://linuxsoft.cern.ch/cern/alma/9/CERN/x86_64 --repo=almacern --nogpgcheck -y install almalinux-release
yum clean all
yum -y install cern-linuxsupport-access cern-get-keytab cern-krb5-conf locmap-release
cern-linuxsupport-access enable
cern-get-keytab

echo "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBMnpQlI4tSwfsfldbH4vj+qks00gThg2AH+TZjgBRQN1n3C1Fa/JPW0cfEWU50b7H0J/SLmjZuHaMK4si8FNYiE= root@lxvmal9.cern.ch" > /etc/ssh/ssh_host_ecdsa_key.pub
cat >> /etc/ssh/ssh_host_ecdsa_key << EOF
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAaAAAABNlY2RzYS
1zaGEyLW5pc3RwMjU2AAAACG5pc3RwMjU2AAAAQQTJ6UJSOLUsH7H5XWx+L4/qpLNNIE4Y
NgB/k2Y4AUUDdZ9wtRWvyT1tHHxFlOdG+x9Cf0i5o2bh2jCuLIvBTWIhAAAAsPcfbzP3H2
8zAAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBMnpQlI4tSwfsfld
bH4vj+qks00gThg2AH+TZjgBRQN1n3C1Fa/JPW0cfEWU50b7H0J/SLmjZuHaMK4si8FNYi
EAAAAgZmSQGF3a9vdvlW6RUF6vSaSww4yMUVp5kyuW2cDdOKQAAAAUcm9vdEBseHZtYWw5
LmNlcm4uY2gBAgME
-----END OPENSSH PRIVATE KEY-----
EOF

chmod 600 /etc/ssh/ssh_host_ecdsa_key
restorecon -F /etc/ssh/ssh_host_ecdsa_key
restorecon -F /etc/ssh/ssh_host_ecdsa_key.pub
fi

# clean up installation
rm -f /tmp/{ks-script-*,yum.log}
rm -f /root/anaconda-{ks,pre,post}.log
rm -f /root/{anaconda,original}-ks.cfg
rm -rf /var/cache/yum/*

# Ironic fix rd.auto
sed -i -e 's/crashkernel/rd.auto crashkernel/g' /etc/default/grub
/usr/sbin/grub2-mkconfig -o /boot/grub2/grub.cfg
#For EFI: grub2-mkconfig -o /boot/efi/EFI/redhat/grub.cfg

# stamp the build
/bin/date "+%Y%m%d %H:%M" > /etc/.BUILDTIME
%end

%packages
@^minimal-environment
-biosdevname
-iprutils
-iwl1000-firmware
-iwl100-firmware
-iwl105-firmware
-iwl135-firmware
-iwl2000-firmware
-iwl2030-firmware
-iwl3160-firmware
-iwl3945-firmware
-iwl4965-firmware
-iwl5000-firmware
-iwl5150-firmware
-iwl6000-firmware
-iwl6000g2a-firmware
-iwl6050-firmware
-iwl7260-firmware
cloud-init
krb5-workstation
rsync
tar
vim-enhanced
yum-utils
git
rpm-build

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end
