#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Install OS instead of upgrade
install
# Use network installation
url --url="http://linuxsoft.cern.ch/cern/centos/7/os/x86_64"

repo --name="CentOS-7 - Base" --baseurl http://linuxsoft.cern.ch/cern/centos/7.6/os/x86_64/
repo --name="CentOS-7 - CERN" --baseurl http://linuxsoft.cern.ch/cern/centos/7/cern/x86_64/
repo --name="CentOS-7 - Updates" --baseurl http://linuxsoft.cern.ch/cern/centos/7/updates/x86_64/
repo --name="CentOS-7 - Extras" --baseurl http://linuxsoft.cern.ch/cern/centos/7/extras/x86_64/
# License agreement
eula --agreed
# Use text mode install
text
# Firewall configuration
firewall --enabled --port=7001:udp,4241:tcp --service=ssh
firstboot --disable
ignoredisk --only-use=vda
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8

# Network information
network  --bootproto=dhcp --device=link --activate
network  --hostname=localhost.localdomain
# Reboot after installation
reboot
# Root password
rootpw --iscrypted [this-is-not-a-root-password]
# SELinux configuration
selinux --enforcing
# System services
services --disabled="kdump,rhsmcertd,wpa_supplicant" --enabled="network,sshd,rsyslog,chronyd"
# Do not configure the X Window System
skipx
# System timezone
timezone Europe/Zurich --isUtc
# System bootloader configuration
bootloader --append="console=ttyS0,115200 console=tty0 crashkernel=auto" --location=mbr --timeout=5 --boot-drive=vda
# Clear the Master Boot Record
zerombr
# Partition clearing information
clearpart --all --initlabel
# Disk partitioning information
part / --fstype="xfs" --ondisk=vda --size=15000 --mkfsoptions="-n ftype=1"

%post --logfile=/root/anaconda-post.log

# lock root account
passwd -d root
passwd -l root

yum -y update
rpm -e linux-firmware

# cannot login on the console anyway ...
sed -i 's/^#NAutoVTs=.*/NAutoVTs=0/' /etc/systemd/logind.conf
# installation time shutdown problem ?
systemctl restart systemd-logind

# disable cloud-init managing network (new in 7.4)
cat >> /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg << EOF
network: {config: disabled}
EOF

# make the system reask for ip .. just in case
cat >> /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
DHCPV6C=yes
PERSISTENT_DHCLIENT=1
NOZEROCONF=1
EOF

sed -i 's|^HWADDR=.*||' /etc/sysconfig/network-scripts/ifcfg-eth0
rm -f /etc/udev/rules.d/70-persistent-net.rules


# set virtual-guest as default profile for tuned
tuned-adm profile virtual-guest

# fix rtc to use utc not local time (INC0974179)
# done via --utc timzezone switch
#timedatectl set-local-rtc 0
#cat >> /etc/adjtime << EOF
#0.0 0 0.0
#0
#UTC
#EOF

# and identical chrony.keys (INC0980266)
echo "" > /etc/chrony.keys

#no tmpfs for /tmp."
systemctl mask tmp.mount

# root - enabled, cloud user - disabled.
if [ -e /etc/cloud/cloud.cfg ]; then
    sed -i 's|^disable_root.*|disable_root: 0|' /etc/cloud/cloud.cfg
    sed -i 's|^ssh_deletekeys.*|ssh_deletekeys: 0|' /etc/cloud/cloud.cfg
    sed -i 's|\- default||' /etc/cloud/cloud.cfg
    sed -i 's|^users:||' /etc/cloud/cloud.cfg

    # fix cloud-init (INC0974035 RH 01594925)
    sed -i -e 's/\&\s\~/\& stop/' /etc/rsyslog.d/21-cloudinit.conf
    # and fix it again !
    sed -i -e 's/\[CLOUDINIT\]/cloud-init/' /etc/rsyslog.d/21-cloudinit.conf
    sed -i -e 's/syslogtag/programname/' /etc/rsyslog.d/21-cloudinit.conf
    sed -i -e 's/cloud-init.log/cloud-init-output.log/' /etc/rsyslog.d/21-cloudinit.conf
fi

# default rpm keys imported.
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-cern
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7

# enable locmap default modules
locmap --enable afs
locmap --enable kerberos

locmap --configure afs

cern-linuxsupport-access enable

echo "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBBr3SZgmH4DPqHbUGaMtqucKwNXiARQdMh07hfrRaJNaMAyP3vvjUi1FpgDwFUK+9UUc0hba3vXYPw3xlCD5cL0=" > /etc/ssh/ssh_host_ecdsa_key.pub
cat >> /etc/ssh/ssh_host_ecdsa_key << EOF
-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIH16eltavekg1A58zZZ2ciX1t65fiYTesApe2pKmlfcroAoGCCqGSM49
AwEHoUQDQgAEGvdJmCYfgM+odtQZoy2q5wrA1eIBFB0yHTuF+tFok1owDI/e++NS
LUWmAPAVQr71RRzSFtre9dg/DfGUIPlwvQ==
-----END EC PRIVATE KEY----
EOF
chmod 600 /etc/ssh/ssh_host_ecdsa_key
restorecon -F /etc/ssh/ssh_host_ecdsa_key
restorecon -F /etc/ssh/ssh_host_ecdsa_key.pub

cern-get-keytab

# add repo files for cloud, virt, storage, sclo
for toplevel in cloud virt storage sclo
do
  for repo in `curl -s  http://linuxsoft.cern.ch/cern/centos/7/$toplevel/x86_64/  |grep "\[DIR" | awk '{print $5}' | cut -d\> -f2 | cut -d\< -f1 | cut -d/ -f1`
  do
    cat >> /etc/yum.repos.d/CentOS-$toplevel.repo << EOF
[$toplevel-$repo]
name=CentOS-7 - $repo
baseurl=http://linuxsoft.cern.ch/cern/centos/7/$toplevel/x86_64/$repo/
gpgcheck=0
enabled=0
[$toplevel-testing-$repo]
name=CentOS-7 - $repo - testing
baseurl=http://linuxsoft.cern.ch/cern/centos/7/$toplevel-testing/x86_64/$repo/
gpgcheck=0
enabled=0
EOF
  done
done

# clean up installation
rm -f /tmp/{ks-script-*,yum.log}
rm -f /root/anaconda-{ks,pre,post}.log
rm -f /root/{anaconda,original}-ks.cfg
rm -rf /var/cache/yum/*

# Ironic fix for RAID1 at boot time (Disabled for now)
/usr/bin/dracut -v --add 'mdraid' --add-drivers 'raid1 raid10 raid5 raid0 linear' --regenerate-all --force

# Ironic fix rd.auto
sed -i -e 's/crashkernel/rd.auto crashkernel/g' /etc/default/grub
/usr/sbin/grub2-mkconfig -o /boot/grub2/grub.cfg
#For EFI: grub2-mkconfig -o /boot/efi/EFI/redhat/grub.cfg

# stamp the build
/bin/date "+%Y%m%d %H:%M" > /etc/.BUILDTIME
%end

%packages
@cern-base
@cern-openafs-client
cern-linuxsupport-access
chrony
cloud-init
cloud-utils-growpart
dracut-config-generic
dracut-norescue
firewalld
grub2
iptables-services
kernel
kexec-tools
krb5-workstation
locmap
mdadm
mesa-libGL
nfs-utils
nscd
ntp
redhat-lsb-core
rsync
sendmail-cf
tar
vim-enhanced
yum-plugin-ovl
yum-utils
-aic94xx-firmware
-alsa-firmware
-alsa-lib
-alsa-tools-firmware
-biosdevname
-iprutils
-ivtv-firmware
-iwl100-firmware
-iwl1000-firmware
-iwl105-firmware
-iwl135-firmware
-iwl2000-firmware
-iwl2030-firmware
-iwl3160-firmware
-iwl3945-firmware
-iwl4965-firmware
-iwl5000-firmware
-iwl5150-firmware
-iwl6000-firmware
-iwl6000g2a-firmware
-iwl6000g2b-firmware
-iwl6050-firmware
-iwl7260-firmware
-iwl7265-firmware
-lcm-firstboot
-libertas-sd8686-firmware
-libertas-sd8787-firmware
-libertas-usb8388-firmware
-linux-firmware
-mcelog
-plymouth
-xorg-x11-font-utils

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'
%end
