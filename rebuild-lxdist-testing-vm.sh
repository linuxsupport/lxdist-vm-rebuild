#!/bin/bash

SERVERS="lxvmcc7 lxvmrh7 lxvmrh8 lxvmrh9 lxvmal8 lxvmal9"

function usage {
cat << EOF
usage: $0 -s [server]

This script is will initiate a rebuild of a given host via openstack/aims2

OPTIONS:
   -s server (or all)  : $SERVERS
   -p                  : disable polling of server rebuild process
   -c                  : disable confirmations
   -r                  : instruct VM to reboot (may be required if VM is 'stuck')
   -f                  : force the association of a kickstart

EOF
exit 1
}

function get_latest_aims_rhel_image {
  echo $(aims2client showimg all |grep $1 |grep 'X86_64' | sort | tail -1 | awk '{print $1}' | sed 's/_X86_64//g')
}

function pre_rebuild {
  hostname=$1
  uuid=`openstack server show ${hostname} -f value -c id 2>/dev/null`
  if [ $? -ne 0 ]; then
    echo "cannot find $hostname in openstack"
    return
  fi

  arch='X86_64'
  if [[ $hostname == *"cc7"* ]]; then
    dist='CC7'
    kickstart='cc7-x86_64.ks'
  elif [[ $hostname == *"rh7"* ]]; then
    dist=$(get_latest_aims_rhel_image 'RHEL_7')
    kickstart='rhel7-x86_64.ks'
  elif [[ $hostname == *"rh8"* ]]; then
    dist='RHEL8'
    kickstart='rhel8-x86_64.ks'
  elif [[ $hostname == *"rh9"* ]]; then
    dist='RHEL9'
    kickstart='rhel9-x86_64.ks'
  elif [[ $hostname == *"al8"* ]]; then
    dist='ALMA8'
    kickstart='alma8-x86_64.ks'
  elif [[ $hostname == *"al9"* ]]; then
    dist='ALMA9'
    kickstart='alma9-x86_64.ks'
  fi
  target="${dist}_${arch}"
  if [ ! -z $reboot ]; then
    if [ -z $confirmations ]; then
      read -p "Are you sure you want to reboot $hostname (y/n)? " choice
      case "$choice" in
        y|Y|yes|YES ) reboot $uuid $hostname;;
        n|N|no|NO ) echo "not rebooting $hostname"; return;;
        * ) echo "invalid choice"; return;;
      esac
    else
      reboot $uuid $hostname
    fi
  else
    if [ -z $confirmations ]; then
      echo "This script will initiate a rebuild of $hostname (openstack uuid: $uuid)"
      echo "The host will be rebuilt as AIMS2 target: $target"
      read -p "Are you sure you want to continue (y/n)? " choice
      case "$choice" in
        y|Y|yes|YES ) rebuild $hostname $target $uuid $kickstart $force;;
        n|N|no|NO ) echo "not rebuilding $1"; return;;
        * ) echo "invalid choice"; return;;
      esac
    else
      rebuild $hostname $target $uuid $kickstart $force
    fi
  fi
}

function reboot {
uuid=$1
hostname=$2
echo "rebooting $uuid ($hostname)"
openstack server reboot --hard $uuid
}

function rebuild {
  hostname=$1
  target=$2
  uuid=$3
  kickstart=$4
  force=$5
  echo "checking entry exists in AIMS2 for ${hostname}"
  aims2client showhost ${hostname} 2>&1 | grep -q "No matching"
  if [ $? -eq 0 ]; then
    echo "${hostname} does not currently exist in AIMS2, adding now"
    aims2client addhost --kickstart ${kickstart} ${hostname}
  fi
  if [ $force -eq 1 ]; then
    echo "-f passed, forcing kickstart registration for ${hostname} (file: ${kickstart})"
    aims2client addhost --kickstart ${kickstart} ${hostname}
  fi
  echo "setting AIMS2 pxeon for ${hostname}"
  aims2client pxeon $hostname $target
  echo "rebuilding $uuid"
  # add artificial sleep to ensure that the AIMs server correctly registers the update
  sleep 40
  openstack server rebuild $uuid
  if [ -z $poll ]; then
    echo "waiting for $hostname to be rebuilt. This will typically take ~10 minutes"
    sleep 60 # give the server rebuild time to reboot the host
    while ! nc -z ${hostname} 22; do   
      sleep 5
    done
    echo "server is up - you may now ssh root@$hostname"
  fi
}

force=0
while getopts "hpcfrs:" option
do
  case $option in
    h)
      usage
      exit 1
      ;;
    s)
      server=$OPTARG
      ;;
    p)
      poll=false
      ;;
    c)
      confirmations=false
      ;;
    r)
      reboot=true
      ;;
    f)
      force=1
      ;;
  esac
done

if [ -z $server ]; then
  usage
  exit 1
fi

unset OS_PROJECT_ID;
unset OS_TENANT_ID;
unset OS_TENANT_NAME;
export OS_PROJECT_NAME="IT Linux Support - Test VMs";

if [ "$server" == "all" ]; then
  for server in $SERVERS
  do
    pre_rebuild $server
  done
else
  pre_rebuild $server
fi
