#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Install OS instead of upgrade
install
# Use network installation
url --url="http://linuxsoft.cern.ch/enterprise/rhel/server/7/latest/x86_64/"

repo --name="RHEL7 - Base" --baseurl http://linuxsoft.cern.ch/enterprise/rhel/server/7/latest/x86_64/
repo --name="RHEL7 - Updates" --baseurl http://linuxsoft.cern.ch/cdn.redhat.com/content/dist/rhel/server/7/7Server/x86_64/os/
# License agreement
eula --agreed
# Use text mode install
text
# Firewall configuration
firewall --enabled --port=7001:udp,4241:tcp --service=ssh
firstboot --disable
ignoredisk --only-use=vda
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8

# Network information
network  --bootproto=dhcp --device=link --activate --noipv6
network  --hostname=localhost.localdomain
# Reboot after installation
reboot
# Root password
rootpw --iscrypted [this-is-not-a-root-password]
# SELinux configuration
selinux --enforcing
# System services
services --disabled="kdump,rhsmcertd,wpa_supplicant" --enabled="network,sshd,rsyslog,chronyd"
# Do not configure the X Window System
skipx
# System timezone
timezone Europe/Zurich --isUtc
# System bootloader configuration
bootloader --append="console=ttyS0,115200 console=tty0 crashkernel=auto" --location=mbr --timeout=5 --boot-drive=vda
# Clear the Master Boot Record
zerombr
# Partition clearing information
clearpart --all --initlabel
# Disk partitioning information
part / --fstype="xfs" --ondisk=vda --size=15000 --mkfsoptions="-n ftype=1"

%post --logfile=/root/anaconda-post.log

# lock root account
passwd -d root
passwd -l root

yum -y update
rpm -e linux-firmware

# cannot login on the console anyway ...
sed -i 's/^#NAutoVTs=.*/NAutoVTs=0/' /etc/systemd/logind.conf
# installation time shutdown problem ?
systemctl restart systemd-logind

# disable cloud-init managing network (new in 7.4)
cat >> /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg << EOF
network: {config: disabled}
EOF

# make the system reask for ip .. just in case
cat >> /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
DHCPV6C=yes
PERSISTENT_DHCLIENT=1
NOZEROCONF=1
EOF

sed -i 's|^HWADDR=.*||' /etc/sysconfig/network-scripts/ifcfg-eth0
rm -f /etc/udev/rules.d/70-persistent-net.rules


# set virtual-guest as default profile for tuned
tuned-adm profile virtual-guest

# fix rtc to use utc not local time (INC0974179)
# done via --utc timzezone switch
#timedatectl set-local-rtc 0
#cat >> /etc/adjtime << EOF
#0.0 0 0.0
#0
#UTC
#EOF

# and identical chrony.keys (INC0980266)
echo "" > /etc/chrony.keys

#no tmpfs for /tmp."
systemctl mask tmp.mount

# root - enabled, cloud user - disabled.
if [ -e /etc/cloud/cloud.cfg ]; then
    sed -i 's|^disable_root.*|disable_root: 0|' /etc/cloud/cloud.cfg
    sed -i 's|^ssh_deletekeys.*|ssh_deletekeys: 0|' /etc/cloud/cloud.cfg
    sed -i 's|\- default||' /etc/cloud/cloud.cfg
    sed -i 's|^users:||' /etc/cloud/cloud.cfg

    # fix cloud-init (INC0974035 RH 01594925)
    sed -i -e 's/\&\s\~/\& stop/' /etc/rsyslog.d/21-cloudinit.conf
    # and fix it again !
    sed -i -e 's/\[CLOUDINIT\]/cloud-init/' /etc/rsyslog.d/21-cloudinit.conf
    sed -i -e 's/syslogtag/programname/' /etc/rsyslog.d/21-cloudinit.conf
    sed -i -e 's/cloud-init.log/cloud-init-output.log/' /etc/rsyslog.d/21-cloudinit.conf
fi

cat >> /etc/yum.repos.d/cern.repo << EOF
[cern]
name=CentOS-7 - CERN
baseurl=http://linuxsoft.cern.ch/cern/centos/7/cern/x86_64/
exlude=centos-release,yum-autoupdate
gpgcheck=0
enabled=1
EOF
curl -o /etc/yum.repos.d/rhel7.repo https://linux.web.cern.ch/rhel/repofiles/rhel7.repo
yum clean all
yum -y install cern-linuxsupport-access cern-get-keytab
cern-linuxsupport-access enable

echo "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBHQn8Ye8Zr6/TL2AZuFSRZ9x+JM+0LYKiomwslsEWnc+jvjF2WcctjAZ0rokfi8dhBUU1mVY7oHvIZvMoEbnLF8=" > /etc/ssh/ssh_host_ecdsa_key.pub
cat >> /etc/ssh/ssh_host_ecdsa_key << EOF
-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIGztrOl+R4rsYyXV+KOXycXpLejduPW+sFsrO5uo6FsmoAoGCCqGSM49
AwEHoUQDQgAEdCfxh7xmvr9MvYBm4VJFn3H4kz7QtgqKibCyWwRadz6O+MXZZxy2
MBnSuiR+Lx2EFRTWZVjuge8hm8ygRucsXw==
-----END EC PRIVATE KEY-----
EOF
chmod 600 /etc/ssh/ssh_host_ecdsa_key
restorecon -F /etc/ssh/ssh_host_ecdsa_key
restorecon -F /etc/ssh/ssh_host_ecdsa_key.pub
fi

cern-get-keytab

# clean up installation
rm -f /tmp/{ks-script-*,yum.log}
rm -f /root/anaconda-{ks,pre,post}.log
rm -f /root/{anaconda,original}-ks.cfg
rm -rf /var/cache/yum/*

# Ironic fix rd.auto
sed -i -e 's/crashkernel/rd.auto crashkernel/g' /etc/default/grub
/usr/sbin/grub2-mkconfig -o /boot/grub2/grub.cfg
#For EFI: grub2-mkconfig -o /boot/efi/EFI/redhat/grub.cfg

# stamp the build
/bin/date "+%Y%m%d %H:%M" > /etc/.BUILDTIME
%end

%packages --nobase
@core
krb5-workstation
git
rpm-build

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end
